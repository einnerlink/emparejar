using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject tile;
    public int[] orden;
    public Color[] colors;

    public GameObject cubeA;
    public GameObject cubeB;

    [Header("Timer")]
    public Text textCount;
    float timer;
    bool isReady = true;

    [Header("Paneles")]
    public GameObject panelGameOver;
    public GameObject panelWinner;

    void Start()
    {
        timer = 120;
        int count = 0;
        Shuffle(orden);

        for (int x = 0; x < 8; x+=2)//Solo en X
        {

            for (int y = 0; y < 8; y+=2)
            {
                GameObject newTile = Instantiate(tile);
                newTile.transform.position = new Vector3(x, y, 0);
                newTile.name = orden[count].ToString();
                newTile.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = colors[orden[count]];
                count++;
            }
        }
    }

    void Shuffle(int[] array)
    {
        int p = array.Length;

        for (int n = p - 1; n > 0; n--)
        {
            int r = Random.Range(0, n);
            int t = array[r];
            array[r] = array[n];
            array[n] = t;
        }
    }

    private void Update()
    {
        if(timer >= 0 && isReady)
        {
            timer -= Time.deltaTime;
            textCount.text = timer.ToString("00");
        }
        else
        {
            panelGameOver.SetActive(true);
            isReady = false;
        }

        if(Input.GetButtonDown("Fire1"))
        {
            Ray ray; //rayo
            RaycastHit hit; //Detecta el objeto
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if(hit.transform.CompareTag("Tile")) //Se obtiene el cubo
                {
                    Debug.Log("Es el tile: " + hit.transform.name);
                    if(cubeA == null)
                    {
                        cubeA = hit.transform.gameObject;
                        hit.transform.GetComponent<BoxCollider>().enabled = false;
                        iTween.RotateTo(hit.transform.gameObject, iTween.Hash("y", 180));
                    }
                    else if (cubeB == null)
                    {
                        cubeB = hit.transform.gameObject;
                        hit.transform.GetComponent<BoxCollider>().enabled = false;
                        iTween.RotateTo(hit.transform.gameObject, iTween.Hash("y", 180));
                        StartCoroutine(CompareTiles());
                    }
                }
            }
        }
    }

    IEnumerator CompareTiles()
    {
        yield return new WaitForSeconds(1);

        //COMPARAR TILES
        //Si son iguales se detruye
        if (cubeA.name == cubeB.name)
        {
            Debug.Log("Son iguales");
            Destroy(cubeA);
            Destroy(cubeB);
            cubeA = null;
            cubeB = null;
        }
        //Si son dirferentes no se destruyen
        else
        {
            Debug.Log("Son diferentes");
            iTween.RotateTo(cubeA.transform.gameObject, iTween.Hash("y", 0));
            iTween.RotateTo(cubeB.transform.gameObject, iTween.Hash("y", 0));
            cubeA.transform.GetComponent<BoxCollider>().enabled = true;
            cubeB.transform.GetComponent<BoxCollider>().enabled = true;
            cubeA = null;
            cubeB = null;
        }
    }
}
