## Nombre
EMPAREJAR (nombre provisional)

## Descripción
Emparejar es un videjuego para Android y PC que consiste en encontrar pares de fichas que coincidan con la misma figura o símbolo, parecido al Mahjong. A medida que se superan los niveles se agregan nuevas fichas y se reduce el tiempo. Contará con una tabla de clasificación que mostrará el ranking de los jugadores.

## Tech
- Unity
- VS
- C#
- Firebase

## Roadmap
- [ ] Tabla de posiciones
- [ ] Login
- [ ] ...
